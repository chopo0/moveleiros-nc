﻿using System.Threading.Tasks;
using Nop.Core.Domain.IpGeolocations;

namespace Nop.Services.IpGeolocations
{
    public interface IIpGeolocationService
    {
        IpGeolocation GetByIp(string ip);
        Task<IpGeolocation> GetIpLocalization(string ip);
        void Insert(IpGeolocation ipGeolocation);
    }
}