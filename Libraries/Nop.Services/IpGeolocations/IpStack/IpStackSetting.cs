﻿using Nop.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.IpGeolocations.IpStack
{
    public class IpStackSetting : ISettings
    {
        public string SecretKey { get; set; }
    }
}
