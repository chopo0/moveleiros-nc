﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.IpGeolocations.IpStack
{

    //{
    //  "ip":"177.190.197.42",
    //  "type":"ipv4",
    //  "continent_code":"SA",
    //  "continent_name":"South America",
    //  "country_code":"BR",
    //  "country_name":"Brazil",
    //  "region_code":"SP",
    //  "region_name":"Sao Paulo",
    //  "city":"S\u00e3o Paulo",
    //  "zip":null,
    //  "latitude":-23.5733,
    //  "longitude":-46.6417,
    //  "location":{
    //    "geoname_id":3448439,
    //    "capital":"Bras\u00edlia",
    //    "languages":[
    //      {
    //        "code":"pt",
    //        "name":"Portuguese",
    //        "native":"Portugu\u00eas"
    //      }
    //    ],
    //    "country_flag":"http:\/\/assets.ipstack.com\/flags\/br.svg",
    //    "country_flag_emoji":"\ud83c\udde7\ud83c\uddf7",
    //    "country_flag_emoji_unicode":"U+1F1E7 U+1F1F7",
    //    "calling_code":"55",
    //    "is_eu":false
    //  }
    //}

    public class IpStackResult
    {
        public string Ip { get; set; }
        public string Type { get; set; }
        public string ContinentCode { get; set; }
        public string ContinentName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string City { get; set; }
    }
}
