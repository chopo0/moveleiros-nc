﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Nop.Services.IpGeolocations.IpStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.IpGeolocations;
using Nop.Services.Configuration;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using RestSharp;
using Nop.Services.Logging;

namespace Nop.Services.IpGeolocations
{
    public class IpGeolocationService : IIpGeolocationService
    {
        private readonly IRepository<IpGeolocation> _ipGeolocationRepository;
        private readonly IRepository<City> _cityRepository;
        private readonly IpStackSetting _ipStackSetting;
        private readonly ILogger _logger;

        public IpGeolocationService(IRepository<IpGeolocation> ipGeolocationRepository,
            IRepository<City> cityRepository,
            IpStackSetting ipStackSetting,
            ILogger logger)
        {
            _ipGeolocationRepository = ipGeolocationRepository;
            _cityRepository = cityRepository;
            _ipStackSetting = ipStackSetting;
            _logger = logger;
        }

        public IpGeolocation GetByIp(string ip)
        {
            return _ipGeolocationRepository.Table.SingleOrDefault(x => x.Ip == ip);
        }

        public void Insert(IpGeolocation ipGeolocation)
        {
            _ipGeolocationRepository.Insert(ipGeolocation);
        }
        
        public async Task<IpGeolocation> GetIpLocalization(string ip)
        {
            // Tenta obter da tabela/cache
            var ipGeolocalization = GetByIp(ip);

            if (ipGeolocalization == null)
            {
                var restRequest = new RestRequest();
                restRequest.Resource = "/" + ip;
                restRequest.AddQueryParameter("access_key", _ipStackSetting.SecretKey);

                IpStackResult result = null;
                try
                {
                    var client = new RestClient();
                    client.BaseUrl = new Uri("http://api.ipstack.com");
                    var response = await client.ExecuteTaskAsync(restRequest);
                    result = JsonConvert.DeserializeObject<IpStackResult>(response.Content,
                        new JsonSerializerSettings
                        {
                            ContractResolver = new DefaultContractResolver()
                            {
                                NamingStrategy = new SnakeCaseNamingStrategy()
                            }
                        });
                }
                catch (Exception e)
                {
                    _logger.InsertLog(Core.Domain.Logging.LogLevel.Error, e.Message, e.StackTrace);
                    return null;
                }

                if (result != null)
                {
                    var foundCity = _cityRepository.Table.FirstOrDefault(x => x.UF == result.RegionCode && x.Description == result.City);

                    if (foundCity != null)
                    {
                        ipGeolocalization = new IpGeolocation()
                        {
                            CityId = foundCity.Id,
                            City = foundCity,
                            Ip = result.Ip,
                            RegisteredOnUtc = DateTime.UtcNow
                        };

                        _ipGeolocationRepository.Insert(ipGeolocalization);
                    }
                    else
                    {
                        _logger.InsertLog(Core.Domain.Logging.LogLevel.Information, "[IpGeo " + ip + "] City not found: " + result.RegionCode + " - " + result.City);
                    }
                }
            }

            return ipGeolocalization;
        }

    }
}
