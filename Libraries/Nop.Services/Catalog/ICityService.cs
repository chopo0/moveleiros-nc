﻿using Nop.Core.Domain.Catalog;
using System.Collections.Generic;

namespace Nop.Services.Catalog
{
    public partial interface ICityService
    {
        IList<City> SearchCities(string city);
        City GetCityById(int cityId);
        City GetCityByUrl(string url);
        IList<City> GetAllCities();
        City GetCityByNameAndState(string name, string state);
        IList<City> GetCitiesByNameAndState(string name, string state);
    }
}
