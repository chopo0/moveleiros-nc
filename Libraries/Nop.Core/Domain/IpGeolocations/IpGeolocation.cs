﻿using Nop.Core.Domain.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.IpGeolocations
{
    public class IpGeolocation : BaseEntity
    {
        public string Ip { get; set; }
        public int CityId { get; set; }
        public virtual City City { get; set; }
        public DateTime RegisteredOnUtc { get; set; }
    }
}
