﻿
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nop.Core.Domain.Livechat
{
    public class LivechatChannelUser : StringKeyBaseEntity
    {
        public string LivechatUserId { get; set; }
        public string LivechatChannelId { get; set; }
		public string Payload { get; set; }
		public DateTime CreatedAt { get; set; }

		// public virtual LivechatChannel LivechatChannel { get; set; }
	}
}
