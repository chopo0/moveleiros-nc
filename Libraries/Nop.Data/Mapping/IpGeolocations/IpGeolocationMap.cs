﻿using Nop.Core.Domain.IpGeolocations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Data.Mapping.IpGeolocations
{
    public partial class IpGeolocationMap : NopEntityTypeConfiguration<IpGeolocation>
    {
        public IpGeolocationMap()
        {
            this.ToTable("IpGeolocation");
            this.HasKey(p => p.Id);
            this.Property(sp => sp.Ip).IsRequired().HasMaxLength(40);
            this.Property(sp => sp.RegisteredOnUtc).IsRequired();

            this.HasRequired(sp => sp.City)
                .WithMany()
                .HasForeignKey(sp => sp.CityId);
        }
    }
}
