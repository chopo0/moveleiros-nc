﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.City
{
    public class AutocompleteModel
    {
        public string Value { get; set; }
        public string ShowText { get; set; }
    }
}