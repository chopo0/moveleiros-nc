﻿class Soixa {

    get(url, callback, headers) {
        let req = new XMLHttpRequest()

        req.onreadystatechange = () => {
            if (req.readyState == XMLHttpRequest.DONE) {
                if (callback) callback(null, req.response)
            }
        }

        req.onerror = (err) => {
            if (callback) callback(err, null)
        }

        req.open('GET', url, true)

        for (key in headers) {
            req.setRequestHeader(key, headers[key])
        }

        req.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
        req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=utf-8')

        req.send(null)
    }

    post(url, data, callback, headers) {
        let req = new XMLHttpRequest()

        req.onreadystatechange = () => {
            if (req.readyState == XMLHttpRequest.DONE) {
                if (callback) callback(null, req.response)
            }
        }

        req.onerror = (err) => {
            if (callback) callback(err, null)
        }

        for (key in headers) {
            req.setRequestHeader(key, headers[key])
        }

        req.open('POST', url, true)
        req.send(data)
    }
}

class FilterRequest {
    constructor() {
        this._axios = new Soixa()

        this.bindEvent()
    }

    bindEvent() {
        window.getProducts = (c, s, f, d) => this.getProducts(c, s, f, d);
        window.getCities = (c, s, f, d) => this.getCities(c, s, f, d);
        window.getIpGeolocation = (s, f, d) => this.getIpGeolocation(s, f, d);
    }

    doGetRequest(url, successCallback, failCallback, doneCallback) {
        this._axios.get(url, (err, data) => {
            if (err) {
                if (failCallback) failCallback(err)
                return
            }

            if (successCallback) successCallback(data)
            if (doneCallback) doneCallback()
        })
    }

    getCities(curl, successCallback, failCallback, doneCallback) {
        let queryString = curl.length > 1 ? curl + '&ajax=true' : curl;
        this.doGetRequest(`/City/GetCitiesByName${queryString}`, successCallback, failCallback, doneCallback);
    }

    getProducts(curl, successCallback, failCallback, doneCallback) {
        let queryString = curl.length > 1 ? curl + '&ajax=true' : curl;
        this.doGetRequest(`/catalog/search${queryString}`, successCallback, failCallback, doneCallback);
    }

    getIpGeolocation(successCallback, failCallback, doneCallback) {
        this.doGetRequest(`/ipgeolocation/get`, successCallback, failCallback, doneCallback);
    }
}

new FilterRequest()