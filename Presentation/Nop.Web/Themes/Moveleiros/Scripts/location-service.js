﻿(function (w) {

    'use strict'

    w.MovScripts = w.MovScripts || {}

    w.initReferenceMap = init

    var loadingElement

    function init() {
        w.MovScripts.getPosition = getPosition

        if (!navigator.geolocation) {
            $('[data-geonotification]').remove()
        }
    }
    
    function getPosition(locationEl, showErrorAlert) {
        var cityEl = locationEl.closest('#City')
        loadingElement = locationEl.siblings('i')
        loadingElement.show()

        if (!navigator.geolocation) {
            getIpGeolocation(ipGeolocationSuccess, null, doneFunction)
            return;
        }
        navigator
            .geolocation
            .getCurrentPosition(getSuccessFunction, getErrorFunction)   
    }

    function ipGeolocationSuccess(data) {
        var objData = JSON.parse(data);
        if (objData.error === false) {
            console.log('ip geolocation - found')
            var cityString = objData.city + ', ' + objData.state;
            $('[data-city]').val(cityString)
            setLastCity(cityString)
            loadingElement.hide()
            w.setFilterTimer()
        } else {
            console.log('ip geolocation - not found')
        }
    }

    function doneFunction() {
        loadingElement.hide()
    }

    function getErrorFunction(err) {
        console.log(err)
        loadingElement.hide()
        getIpGeolocation(ipGeolocationSuccess, null, doneFunction)
    }

    function getSuccessFunction(position) {
        console.log('browser location')
        var lat = position.coords.latitude
        var lng = position.coords.longitude
        var city
        var geocoder = new google.maps.Geocoder()
        var latlng = new google.maps.LatLng(lat, lng)

        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {

                if (results[1]) {
                    //find country name
                    for (var i = 0; i < results[0].address_components.length; i++) {
						for (var b = 0; b < results[0].address_components[i].types.length; b++) {
							console.log(results[0].address_components[i].types[b]);

                            //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate

							if (results[0].address_components[i].types[b] === "locality" || results[0].address_components[i].types[b] === "administrative_area_level_2") {
                                //this is the object you are looking for
                                var city = results[0].address_components[i];
                            }

                            if (results[0].address_components[i].types[b] === "administrative_area_level_1") {
                                //this is the object you are looking for
                                var state = results[0].address_components[i];
                            }
                        }
                    }

                    //city data
                    $('[data-city]').val(city.long_name + ', ' + state.short_name)
                    setLastCity(city.long_name + ', ' + state.short_name)
                    loadingElement.hide()
                    w.setFilterTimer()
                }
            }

            loadingElement.hide()
        })
    }
})(window)