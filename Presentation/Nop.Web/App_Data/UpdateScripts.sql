﻿-- Tabela para armazenar endereços de IP

CREATE TABLE [dbo].[IpGeolocation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CityId] [int] NOT NULL,
	[Ip] [nvarchar](50) NOT NULL,
	[RegisteredOnUtc] [DATETIME] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[IpGeolocation]  WITH CHECK ADD  CONSTRAINT [IpGeolocation_City] FOREIGN KEY([CityId])
REFERENCES [dbo].[City] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[IpGeolocation] CHECK CONSTRAINT [IpGeolocation_City]
GO


