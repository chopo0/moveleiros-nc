﻿using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Web.Models.City;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Web.Controllers
{
    public class CityController : BasePublicController
    {
        private readonly ICityService _cityService;

        public CityController(ICityService cityService)
        {
            _cityService = cityService;
        }

        public ActionResult GetCitiesByName(string cityName)
        {
            var cityAndState = cityName.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            IList<City> cities = null;
            if (cityAndState.Length > 1)
                cities = _cityService.GetCitiesByNameAndState(cityAndState.First().Trim(), cityAndState.Last().Trim());

            if (cities == null)
                cities = _cityService.GetCitiesByNameAndState(cityName.Trim(), string.Empty);

            if (cities == null)
                cities = new List<City>();

            var model = new List<AutocompleteModel>();

            var cityNameNormalized = cityAndState.Length > 1 ? $"{cityAndState.First().Trim()}, {cityAndState.Last().Trim()}" : cityName;
            foreach (var city in cities)
            {
                var value = $"{city.Description}, {city.UF}";
                var bold = cityNameNormalized.Length > 0 ? value.Replace(cityNameNormalized, $"<strong>{cityNameNormalized}</strong>") : value;
                model.Add(new AutocompleteModel
                {
                    Value = value,
                    ShowText = bold
                });
            }

            return PartialView(model);
        }
    }
}