﻿using System.Web.Mvc;
using Nop.Web.Framework.Security;

namespace Nop.Web.Controllers
{
    public partial class HomeController : BasePublicController
    {
        [NopHttpsRequirement(SslRequirement.NoMatter)]
        public virtual ActionResult Index()
        {
            return View();
        }
    }
}
