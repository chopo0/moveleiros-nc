﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Services.IpGeolocations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Nop.Web.Controllers
{
    public class IpGeolocationController : BasePublicController
    {
        private readonly IIpGeolocationService _ipGeolocationService;
        private readonly IWebHelper _webHelper;
        private readonly ICacheManager _cacheManager;

        public IpGeolocationController(IIpGeolocationService ipGeolocationService,
            IWebHelper webHelper,
            ICacheManager cacheManager)
        {
            _ipGeolocationService = ipGeolocationService;
            _webHelper = webHelper;
            _cacheManager = cacheManager;
        }

        public async Task<JsonResult> Get()
        {
            var ip = _webHelper.GetCurrentIpAddress();

            // Mantém em memória por 60 min
            return await _cacheManager.Get("Ip." + ip, 60, async () =>
            {
                var location = await _ipGeolocationService.GetIpLocalization(ip);

                if (location != null)
                    return Json(new
                    {
                        error = false,
                        city = location.City.Description,
                        state = location.City.UF
                    }, JsonRequestBehavior.AllowGet);

                return Json(new
                {
                    error = true
                }, JsonRequestBehavior.AllowGet);
            });
        }
    }
}